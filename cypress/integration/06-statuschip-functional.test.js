describe("StatusChip", () => {
  it("has retry functionality", () => {
    cy.visit(
      "http://localhost:6006/iframe.html?id=components-statuschip--error",
      () => {
        const stub = cy.stub();
        cy.on("window:alert", stub);
        cy.find("Please try again")
          .click()
          .then(() => {
            expect(stub.getCall(0)).to.be.called();
          });
      }
    );
  });
});
