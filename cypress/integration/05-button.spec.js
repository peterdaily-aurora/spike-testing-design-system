describe("Button", () => {
  it("should render the button", () => {
    cy.visit("http://localhost:6006/iframe.html?id=components-button--primary");
    cy.get("button").should("contain.text", "Press me");
  });
});
