import React from "react";
import { StatusChip } from "../src/StatusChip";

export default {
  title: "Components/StatusChip",
  component: StatusChip,
};

const Template = (args) => (
  <StatusChip message="The message contents" {...args} />
);

export const Basic = Template.bind({});

export const Loading = Template.bind({});
Loading.args = {
  title: "Loading things...",
  message: "Please wait",
  type: "loading",
};

export const Error = Template.bind({});
Error.args = {
  title: "Something went wrong",
  message: "Extra info about the error here",
  type: "error",
  retry: () => alert("Retrying"),
};

export const Success = Template.bind({});
Success.args = {
  title: "Success",
  message: "Something worked",
  type: "success",
};
