import React from "react";
import { Button } from "../src/Button"

export default {
  title: "Components/Button",
  component: Button
}

const Template = args => <Button {...args}>Press me</Button>;

export const Primary = Template.bind({});

export const Secondary = Template.bind({});
Secondary.args = { color: "secondary" }