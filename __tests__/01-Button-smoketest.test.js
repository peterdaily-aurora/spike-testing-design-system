import React from "react";
import { render } from "@testing-library/react";
import { Button } from "../src/Button";
import "@testing-library/jest-dom";

test("renders Button with the correct test", () => {
  const btnText = "TEST_TEXT";
  const { getByRole } = render(<Button>{btnText}</Button>);
  expect(getByRole("button")).toHaveTextContent(btnText);
});
