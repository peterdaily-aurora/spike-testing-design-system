import React from "react";
import renderer from "react-test-renderer";
import { Button } from "../src/Button";
test("Renders the button", () => {
  const component = renderer.create(<Button>My test button</Button>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
