import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { StatusChip } from "../src/StatusChip";
import "@testing-library/jest-dom";

test("it renders the Basic StatusChip", () => {
  const basicMsg = "BASIC_MSG";
  const { getAllByText } = render(<StatusChip message={basicMsg} />);
  expect(getAllByText(basicMsg)).toHaveLength(1);
});

test("it renders the StatusChip title correctly", () => {
  const title = "TEST_TITLE";
  const msg = "TEST_MSG";
  const { getByTestId } = render(<StatusChip message={msg} title={title} />);
  expect(getByTestId("title")).toHaveTextContent(title);
});

test("the Error type of StatusChip has retry functionality", () => {
  const { getByText } = render(
    <StatusChip type="error" message="test" retry={() => alert("Retrying")} />
  );
  const alertMock = jest.spyOn(window, "alert").mockImplementation();
  fireEvent.click(getByText("Please try again"));
  expect(alertMock).toHaveBeenCalledTimes(1);
});
