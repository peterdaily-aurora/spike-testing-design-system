import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import { composeStories } from "@storybook/testing-react";
import * as stories from "../stories/StatusChip.stories.jsx";

// this imports the component with the same setup (eg, props, decorators) as the Storybook story
const { Success } = composeStories(stories);

it("renders the success StatusChip correctly", () => {
  const { getByTestId } = render(<Success />);
  expect(getByTestId("title")).toHaveTextContent("Success");
});
