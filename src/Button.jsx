import React from "react";
import { Button as MuiButton } from "@material-ui/core";

export const Button = (props) => {
  return <MuiButton variant="contained" color="primary" {...props} />;
};
